#include "helper.h"
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
void exit_program();
void print_id();
void print_uptime();
int main () {
	char current_choice[2] = "";
	current_choice[0] = '1';
	fun_desc choice_exit = {"exit_program", exit_program};
	fun_desc choice_id = {"print_id", print_id};
	fun_desc choice_uptime = {"print_uptime", print_uptime};
	fun_desc menu[3] = {choice_exit, choice_id, choice_uptime};
	while(current_choice[0])
		{
	syscall(SYS_write, STDIN_FILENO, "\n1 - Exit. 2 - Display process ID. 3 - Machine uptime & proclist \n", slen("\n1 - Exit. 2 - Display process ID. 3 - Machine uptime & proclist \n"));
	syscall(SYS_read,STDIN_FILENO,current_choice,2);
			if(current_choice[0] > '0' && current_choice[0] < '4') {
			switch(current_choice[0])
				{
					case '1':
						exit_program();
					case '2':
						print_id();
					case '3':
						print_uptime();
				}
				
			}
			else
					{
				syscall(SYS_write, STDOUT_FILENO, "Bad input", slen("Bad input"));
				exit_program();
					}
		}
	
	return 0;
}
void exit_program() {
	syscall(SYS_exit, 0);
}
void print_id() {
	syscall(SYS_write, STDOUT_FILENO, __itoa(getuid()), slen(__itoa(getuid())));
}
void print_uptime() {
	struct sysinfo system;
	sysinfo(&system);
	syscall(SYS_write, STDOUT_FILENO, __itoa(system.uptime), slen(__itoa(system.uptime)));
	syscall(SYS_write, STDOUT_FILENO, "\n Proclist: \n", slen("\n Proclist: \n"));
	syscall(SYS_write, STDOUT_FILENO, __itoa(system.procs), slen(__itoa(system.procs)));
}
