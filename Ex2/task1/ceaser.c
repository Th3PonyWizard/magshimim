#include "ceaser.h"
#include <string.h>
#include <malloc.h>

char shift_letter(char letter, int offset) {
	int i;
	if(offset > 0) {
		for(i = 0;i < offset;i++) {
			if(letter != 'z')
			{
				letter+=1;
			}
			else {
			    letter = 'z';
			}
		}
	}
	else {
		for(i = offset;i < 0;i++) {
			if(letter != 'a')
			{
				letter=-1;
			}
			else
			{
			    letter = 'a'; 
			}
		}
	}
	return letter;
}

char * shift_string(char * input, int offset)
{
	int length;
	int i;
	length = strlen(input);
	char* encrypted = (char *)calloc(sizeof(char),length * 1);
	for (i = 0; i < length; ++i)
		encrypted[i] = shift_letter(input[i],offset);

	return encrypted;
}
