#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <linux/limits.h>
#include <fcntl.h>
#include "LineParser.h"

void execute(cmdLine *pCmdLine, int fdin, int fdout);
int main()
{
	char mypipe_message_sent[] = "magshimim"; //Messsage parent process should send to child process
	int Child; //childpid
	int fdin, fdout; //Redirection variables
	char mypipe_message_got[100];
	int fd[2], num, //File descriptor for PIPE
		i, //for the myecho command loop
		AllOk = 1, //Boolearn workaround for executing a program out of /bin/ Ex: ls -a
		IsPipe = 0; //Boolean workaround for mypipe program
	/*PATH_MAX: Constant pre-defined in <linux/limits.h>.*/
	char command_input[PATH_MAX], buf[4096];
	/*Calling cmdLine struct in myshdell, linking it to myshell2 to form a pointing connection. cmdLine includes variables for all arguments and their setting*/
	cmdLine myshell, *myshell2 = &myshell;
	/*As long as user didn't input quit, keep going. Double check with if() condition to aviod pointless calculation
	 * at the expense of more code lines*/
	 	
	while(!strncmp(command_input, "quit" , 4) == 0)
	{
		getcwd(buf,sizeof(buf));
		printf("%s: ",buf); /*Getting & Printing current directory. Should appear at every line*/
		fgets(command_input,sizeof(command_input),stdin);
		myshell2 = parseCmdLines(command_input);
		if(strncmp(command_input, "quit" , 4) == 0) {
			printf("Quitting.\n");
			exit(0);
		}
		if(strncmp(command_input, "\n" , 1) == 0) {continue;}
		if(strncmp(command_input, "myecho", 6) == 0)
		{
			for(i = 1;i < myshell2->argCount;i++)
			{
				printf("%s ", myshell2->arguments[i]);
			}
		printf("\n");
		AllOk = 0;
		}
		if(strncmp(command_input, "mypipe", 6) == 0)
		{
			IsPipe = 1;
			AllOk = 0;
		}
		if(strncmp(command_input, "cd", 2) == 0)
		{
			if(chdir(myshell2->arguments[1])) { perror("Failed opening directory"); }
			AllOk = 0;
		}
	if(IsPipe == 1)
		{
				pipe(fd);
				Child = fork();
				if(Child >= 0)
				{
				if(Child == 0)
					{
						/*Identified Child process, which shouldn't be reading anything*/
						close(fd[0]);
						write(fd[1], mypipe_message_sent, (strlen(mypipe_message_sent)+1));
						exit(0);
					}
				else
					{
						/*Identified Parent process, which shoudln't be writing anything*/
						close(fd[1]);
						num = read(fd[0], mypipe_message_got, sizeof(mypipe_message_got));
						printf("Received string: %s\n", mypipe_message_got);
						exit(0);
					}
				}
				else {
					printf("fork failed!");
				}
		}
		if(AllOk == 1)
		{
			Child = fork();
			if(Child >= 0)
				{		
					if(Child == 0)
						{
						execute(myshell2, fdin, fdout);
						exit(0);
						}
					else
						{
					waitpid(-1,&Child,0); //Child process
						}
				}
				else
				{
					perror("The fork HAS FAILED!");
					exit(1);
				}	
		}

	}
	/*Freeing memory. Outside while()?*/
	freeCmdLines(myshell2);
	return 0;
}
void execute(cmdLine *pCmdLine, int fdin, int fdout)
{
	fdin = 0;
	fdout = 0;
	/*execv() returns -1 when fails and doesn't return upon sucess, it actually returns another process image..*/
	/*execv() is replacing the current process (The one we're executing, ./main one) with the process the command spawns, resulting
	 * in the closure of the program right after we're done with the spawned process*/
	 /*Replacing execv() with execvp() which already includes the /bin/ path as the first argument, making writing the full path unneccesary*/
		if(pCmdLine->OutputRedirect) {
			close(1);
			fdout = open(pCmdLine->OutputRedirect,O_WRONLY);
			dup(fdout);
			}
		if(pCmdLine->InputRedirect) {
			close(0);
			fdin = open(pCmdLine->inputRedirect,O_RDONLY);
			dup(fdin);
			}
	if((execvp(pCmdLine->arguments[0],pCmdLine->arguments)) == -1)
	{
		perror("Well, shit.\nError");
	}
}
