#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int main (int agrc, char *argv[]) {
	char ** pointy = argv;
	DWORD PID_INPUT = atoi(pointy[1]);
	HANDLE OurProc = OpenProcess(PROCESS_TERMINATE, TRUE, PID_INPUT);
	if (!OurProc) {
		printf("OurProc has failed: %d", GetLastError());
	}
	if(!TerminateProcess(OurProc, 1)) {
		printf("TerminateProcess has failed: %d", GetLastError());

	}
	CloseHandle(OurProc);
}